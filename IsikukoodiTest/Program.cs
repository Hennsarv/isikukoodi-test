﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IsikukoodiTest
{
    class Program
    {
        static void Main(string[] args)
        {
            string isikukood = "35503070211";
            Isikukood ik = new Isikukood(isikukood);
            Console.WriteLine(ik.Sünniaeg);
            Console.WriteLine(ik.Sugu);
            Console.WriteLine(ik.Vanus);
        }
    }
    class Isikukood
    {
        string _Isikukood;
        public Isikukood(string isikukood) => _Isikukood = isikukood;
        public DateTime Sünniaeg => new DateTime(
            1800 + ((_Isikukood[0] - '1') / 2) * 100 +  // see rida vb kõige segasem
            int.Parse(_Isikukood.Substring(1, 2)),
            int.Parse(_Isikukood.Substring(3, 2)),
            int.Parse(_Isikukood.Substring(5, 2)));
        public Sugu Sugu => (Sugu)(_Isikukood[0] % 2);
        public int Vanus
        {
            get
            {
                int d = DateTime.Now.Year - Sünniaeg.Year;
                if (Sünniaeg.AddYears(d) > DateTime.Now.Date) d--; return d;
            }
        }
    }

    enum Sugu { Naine, Mees};
}
